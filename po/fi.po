# Finnish translation for ubuntu-docviewer-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-docviewer-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-docviewer-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-02-05 23:40+0100\n"
"PO-Revision-Date: 2020-02-11 14:21+0000\n"
"Last-Translator: Oi Suomi On! <oisuomion@protonmail.com>\n"
"Language-Team: Finnish <https://translate.ubports.com/projects/ubports/"
"docviewer-app/fi/>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2017-04-05 07:48+0000\n"

#: ../src/app/qml/common/CommandLineProxy.qml:49
msgid "Some of the provided arguments are not valid."
msgstr "Jotkin annetut argumentit eivät ole kelvollisia."

#: ../src/app/qml/common/CommandLineProxy.qml:58
msgid "Open ubuntu-docviewer-app displaying the selected file"
msgstr "Avaa ubuntu-docviewer-app näyttäen valittu tiedosto"

#: ../src/app/qml/common/CommandLineProxy.qml:65
msgid "Run fullscreen"
msgstr "Suorita koko näytöllä"

#: ../src/app/qml/common/CommandLineProxy.qml:71
msgid "Open ubuntu-docviewer-app in pick mode. Used for tests only."
msgstr ""
"Avaa ubuntu-docviewer-sovellus poimintatilassa. Tarkoitettu käytettäväksi "
"vain testaamiseen."

#: ../src/app/qml/common/CommandLineProxy.qml:77
msgid ""
"Show documents from the given folder, instead of ~/Documents.\n"
"The path must exist prior to running ubuntu-docviewer-app"
msgstr ""
"Näytä asiakirjat annetusta kansiosta, ~/Asiakirjat sijasta.\n"
"Polun on oltava olemassa ennen ubuntu-docviewer-sovelluksen ajamista"

#: ../src/app/qml/common/DetailsPage.qml:26
#: ../src/app/qml/loView/LOViewDefaultHeader.qml:107
#: ../src/app/qml/pdfView/PdfView.qml:235
#: ../src/app/qml/textView/TextViewDefaultHeader.qml:69
msgid "Details"
msgstr "Tiedot"

#: ../src/app/qml/common/DetailsPage.qml:42
msgid "File"
msgstr "Tiedosto"

#: ../src/app/qml/common/DetailsPage.qml:47
msgid "Location"
msgstr "Sijainti"

#: ../src/app/qml/common/DetailsPage.qml:52
msgid "Size"
msgstr "Koko"

#: ../src/app/qml/common/DetailsPage.qml:57
msgid "Created"
msgstr "Luotu"

#: ../src/app/qml/common/DetailsPage.qml:62
msgid "Last modified"
msgstr "Viimeksi muokattu"

#: ../src/app/qml/common/DetailsPage.qml:69
msgid "MIME type"
msgstr "MIME-tyyppi"

#: ../src/app/qml/common/ErrorDialog.qml:23
msgid "Error"
msgstr "Virhe"

#: ../src/app/qml/common/ErrorDialog.qml:26
#: ../src/app/qml/common/PickImportedDialog.qml:54
#: ../src/app/qml/common/RejectedImportDialog.qml:38
#: ../src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:31
#: ../src/app/qml/documentPage/SortSettingsDialog.qml:53
msgid "Close"
msgstr "Sulje"

#: ../src/app/qml/common/PickImportedDialog.qml:29
msgid "Multiple documents imported"
msgstr "Useita asiakirjoja tuotu"

#: ../src/app/qml/common/PickImportedDialog.qml:30
msgid "Choose which one to open:"
msgstr "Valitse mikä niistä avataan:"

#: ../src/app/qml/common/RejectedImportDialog.qml:28
msgid "File not supported"
msgid_plural "Files not supported"
msgstr[0] "Tiedosto ei ole tuettu"
msgstr[1] "Tiedostot eivät ole tuettuja"

#: ../src/app/qml/common/RejectedImportDialog.qml:29
msgid "Following document has not been imported:"
msgid_plural "Following documents have not been imported:"
msgstr[0] "Seuraavaa asiakirjaa ei tuotu:"
msgstr[1] "Seuraavia asiakirjoja ei tuotu:"

#: ../src/app/qml/common/UnknownTypeDialog.qml:27
msgid "Unknown file type"
msgstr "Tuntematon tiedostotyyppi"

#: ../src/app/qml/common/UnknownTypeDialog.qml:28
msgid ""
"This file is not supported.\n"
"Do you want to open it as a plain text?"
msgstr ""
"Tiedosto ei ole tuettu.\n"
"Haluatko avata sen \"pelkkä teksti\"-muodossa?"

#: ../src/app/qml/common/UnknownTypeDialog.qml:38
#: ../src/app/qml/documentPage/DeleteFileDialog.qml:55
#: ../src/app/qml/documentPage/DocumentPagePickModeHeader.qml:28
#: ../src/app/qml/loView/LOViewGotoDialog.qml:55
#: ../src/app/qml/pdfView/PdfView.qml:180
#: ../src/app/qml/pdfView/PdfViewGotoDialog.qml:51
msgid "Cancel"
msgstr "Keskeytä"

#: ../src/app/qml/common/UnknownTypeDialog.qml:44
msgid "Yes"
msgstr "Kyllä"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: ../src/app/qml/common/utils.js:22
#, qt-format
msgid "%1 GB"
msgstr "%1 Gt"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: ../src/app/qml/common/utils.js:26
#, qt-format
msgid "%1 MB"
msgstr "%1 Mt"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: ../src/app/qml/common/utils.js:30
#, qt-format
msgid "%1 kB"
msgstr "%1 kt"

#. TRANSLATORS: %1 is the size of a file, expressed in byte
#: ../src/app/qml/common/utils.js:33
#, qt-format
msgid "%1 byte"
msgstr "%1 tavu"

#: ../src/app/qml/documentPage/DeleteFileDialog.qml:39
msgid "Delete file"
msgstr "Poista tiedosto"

#: ../src/app/qml/documentPage/DeleteFileDialog.qml:40
#, qt-format
msgid "Delete %1 file"
msgid_plural "Delete %1 files"
msgstr[0] "Poista %1 tiedosto"
msgstr[1] "Poista %1 tiedostoa"

#: ../src/app/qml/documentPage/DeleteFileDialog.qml:41
#: ../src/app/qml/documentPage/DeleteFileDialog.qml:42
msgid "Are you sure you want to permanently delete this file?"
msgid_plural "Are you sure you want to permanently delete these files?"
msgstr[0] "Haluatko varmasti poistaa tämän tiedoston pysyvästi?"
msgstr[1] "Haluatko varmasti poistaa nämä tiedostot pysyvästi?"

#: ../src/app/qml/documentPage/DeleteFileDialog.qml:61
#: ../src/app/qml/documentPage/DocumentDelegateActions.qml:25
#: ../src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:53
msgid "Delete"
msgstr "Poista"

#: ../src/app/qml/documentPage/DocumentDelegateActions.qml:44
msgid "Share"
msgstr "Jaa"

#: ../src/app/qml/documentPage/DocumentEmptyState.qml:27
msgid "No documents found"
msgstr "Asiakirjoja ei löytynyt"

#: ../src/app/qml/documentPage/DocumentEmptyState.qml:28
msgid ""
"Connect your device to any computer and simply drag files to the Documents "
"folder or insert removable media containing documents."
msgstr ""
"Yhdistä laitteesi tietokoneeseen ja vedä tiedostoja Asiakirjat-kansioon. "
"Vaihtoehtoisesti liitä asiakirjoja sisältävä erillinen tallennusväline."

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:103
#, qt-format
msgid "Today, %1"
msgstr "Tänään, %1"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:108
#, qt-format
msgid "Yesterday, %1"
msgstr "Eilen, %1"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:115
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:134
msgid "yyyy/MM/dd hh:mm"
msgstr "vuosi/kk/pä hh:mm"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:128
msgid "dddd, hh:mm"
msgstr "dddd, hh.mm"

#: ../src/app/qml/documentPage/DocumentPage.qml:23
#: /tmp/lok-qml-async-imageprovider-build/po/com.ubuntu.docviewer.desktop.in.in.h:3
msgid "Documents"
msgstr "Asiakirjat"

#: ../src/app/qml/documentPage/DocumentPageDefaultHeader.qml:29
msgid "Search..."
msgstr "Etsi..."

#: ../src/app/qml/documentPage/DocumentPageDefaultHeader.qml:36
msgid "Sorting settings..."
msgstr "Järjestysasetukset..."

#: ../src/app/qml/documentPage/DocumentPagePickModeHeader.qml:41
msgid "Switch to single column list"
msgstr "Vaihda yhden sarakkeen luetteloon"

#: ../src/app/qml/documentPage/DocumentPagePickModeHeader.qml:41
msgid "Switch to grid"
msgstr "Vaihda ruudukkoon"

#: ../src/app/qml/documentPage/DocumentPagePickModeHeader.qml:49
msgid "Pick"
msgstr "Valitse"

#: ../src/app/qml/documentPage/DocumentPageSearchHeader.qml:27
msgid "Back"
msgstr "Palaa"

#: ../src/app/qml/documentPage/DocumentPageSearchHeader.qml:47
msgid "search in documents..."
msgstr "etsi asiakirjoista..."

#: ../src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:40
msgid "Select None"
msgstr "Ei valintaa"

#: ../src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:40
msgid "Select All"
msgstr "Valitse kaikki"

#: ../src/app/qml/documentPage/SearchEmptyState.qml:24
msgid "No matching document found"
msgstr "Vastaavaa asiakirjaa ei löytynyt"

#: ../src/app/qml/documentPage/SearchEmptyState.qml:26
msgid ""
"Please ensure that your query is not misspelled and/or try a different query."
msgstr ""
"Varmista, ettei hakusi sisällä kirjoitusvirhettä ja/tai yritä eri "
"hakuehdoilla."

#: ../src/app/qml/documentPage/SectionHeader.qml:13
msgid "Today"
msgstr "Tänään"

#: ../src/app/qml/documentPage/SectionHeader.qml:16
msgid "Yesterday"
msgstr "Eilen"

#: ../src/app/qml/documentPage/SectionHeader.qml:19
msgid "Earlier this week"
msgstr "Aiemmin tällä viikolla"

#: ../src/app/qml/documentPage/SectionHeader.qml:22
msgid "Earlier this month"
msgstr "Aiemmin tässä kuussa"

#: ../src/app/qml/documentPage/SectionHeader.qml:24
msgid "Even earlier..."
msgstr "Vielä aiemmin..."

#: ../src/app/qml/documentPage/SharePage.qml:23
msgid "Share to"
msgstr "Jaa"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:26
msgid "Sorting settings"
msgstr "Järjestysasetukset"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:31
msgid "Sort by date (Latest first)"
msgstr "Järjestä päivän mukaan (uusin ensin)"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:32
msgid "Sort by name (A-Z)"
msgstr "Järjestä nimen mukaan (A-Ö)"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:33
msgid "Sort by size (Smaller first)"
msgstr "Järjestä koon mukaan (pienin ensin)"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:47
msgid "Reverse order"
msgstr "Käänteinen järjestys"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:51
#: ../src/app/qml/textView/TextView.qml:43
msgid "Loading..."
msgstr "Ladataan..."

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:55
msgid "LibreOffice text document"
msgstr "LibreOffice-tekstiasiakirja"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:57
msgid "LibreOffice spread sheet"
msgstr "LibreOffice-laskentataulukko"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:59
msgid "LibreOffice presentation"
msgstr "LibreOffice-esitys"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:61
msgid "LibreOffice Draw document"
msgstr "LibreOffice-piirros"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:63
msgid "Unknown LibreOffice document"
msgstr "Tuntematon LibreOffice-asiakirja"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:65
msgid "Unknown type document"
msgstr "Tuntematon asiakirjamuoto"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:85
msgid "Go to position..."
msgstr "Siirry sijaintiin..."

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:100
#: ../src/app/qml/pdfView/PdfView.qml:228
#: ../src/app/qml/textView/TextViewDefaultHeader.qml:63
msgid "Disable night mode"
msgstr "Poista yötila käytöstä"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:100
#: ../src/app/qml/pdfView/PdfView.qml:228
#: ../src/app/qml/textView/TextViewDefaultHeader.qml:63
msgid "Enable night mode"
msgstr "Ota yötila käyttöön"

#: ../src/app/qml/loView/LOViewGotoDialog.qml:30
msgid "Go to position"
msgstr "Valitse sijainti"

#: ../src/app/qml/loView/LOViewGotoDialog.qml:31
msgid "Choose a position between 1% and 100%"
msgstr "Valitse sijainti välillä 1 % ja 100 %"

#: ../src/app/qml/loView/LOViewGotoDialog.qml:62
#: ../src/app/qml/pdfView/PdfViewGotoDialog.qml:58
msgid "GO!"
msgstr "Siirry!"

#: ../src/app/qml/loView/LOViewPage.qml:167
msgid "LibreOffice binaries not found."
msgstr "LibreOffice-binääritiedostoja ei löytynyt."

#: ../src/app/qml/loView/LOViewPage.qml:170
msgid "Error while loading LibreOffice."
msgstr "Virhe LibreOfficea ladatessa."

#: ../src/app/qml/loView/LOViewPage.qml:173
msgid ""
"Document not loaded.\n"
"The requested document may be corrupt or protected by a password."
msgstr ""
"Asiakirjaa ei ladattu.\n"
"Asiakirja voi olla rikki tai suojattu salasanalla."

#: ../src/app/qml/loView/LOViewPage.qml:228
msgid "This sheet has no content."
msgstr "Tässä taulukossa ei ole sisältöä."

#. TRANSLATORS: 'LibreOfficeKit' is the name of the library used by
#. Document Viewer for rendering LibreOffice/MS-Office documents.
#. Ref. https://docs.libreoffice.org/libreofficekit.html
#: ../src/app/qml/loView/Splashscreen.qml:45
msgid "Powered by LibreOfficeKit"
msgstr "Voimanlähteenä LibreOfficeKit"

#. TRANSLATORS: Please don't add any space between "Sheet" and "%1".
#. This is the default name for a sheet in LibreOffice.
#: ../src/app/qml/loView/SpreadsheetSelector.qml:64
#, qt-format
msgid "Sheet%1"
msgstr "Taulukko%1"

#: ../src/app/qml/loView/ZoomSelector.qml:122
msgid "Fit width"
msgstr "Sovita leveys"

#: ../src/app/qml/loView/ZoomSelector.qml:123
msgid "Fit height"
msgstr "Sovita korkeuteen"

#: ../src/app/qml/loView/ZoomSelector.qml:124
msgid "Automatic"
msgstr "Automaattinen"

#. TRANSLATORS: "Contents" refers to the "Table of Contents" of a PDF document.
#: ../src/app/qml/pdfView/PdfContentsPage.qml:31
#: ../src/app/qml/pdfView/PdfView.qml:153
msgid "Contents"
msgstr "Sisältö"

#. TRANSLATORS: the first argument (%1) refers to the page currently shown on the screen,
#. while the second one (%2) refers to the total pages count.
#: ../src/app/qml/pdfView/PdfPresentation.qml:51
#: ../src/app/qml/pdfView/PdfView.qml:56
#, qt-format
msgid "Page %1 of %2"
msgstr "Sivu %1/%2"

#: ../src/app/qml/pdfView/PdfView.qml:203
msgid "Search"
msgstr "Etsi"

#: ../src/app/qml/pdfView/PdfView.qml:213
msgid "Go to page..."
msgstr "Siirry sivulle..."

#: ../src/app/qml/pdfView/PdfView.qml:221
msgid "Presentation"
msgstr "Esitys"

#: ../src/app/qml/pdfView/PdfViewGotoDialog.qml:26
msgid "Go to page"
msgstr "Siirry sivulle"

#: ../src/app/qml/pdfView/PdfViewGotoDialog.qml:27
#, qt-format
msgid "Choose a page between 1 and %1"
msgstr "Valitse sivu väliltä 1 ja %1"

#: ../src/app/qml/ubuntu-docviewer-app.qml:114
msgid "File does not exist."
msgstr "Tiedostoa ei ole olemassa."

#. TRANSLATORS: This string is used for renaming a copied file,
#. when a file with the same name already exists in user's
#. Documents folder.
#.
#. e.g. "Manual_Aquaris_E4.5_ubuntu_EN.pdf" will become
#. "Manual_Aquaris_E4.5_ubuntu_EN (copy 2).pdf"
#.
#. where "2" is given by the argument "%1"
#.
#: ../src/plugin/file-qml-plugin/docviewerutils.cpp:111
#, qt-format
msgid "copy %1"
msgstr "kopio %1"

#: /tmp/lok-qml-async-imageprovider-build/po/com.ubuntu.docviewer.desktop.in.in.h:1
msgid "Document Viewer"
msgstr "Asiakirjakatselin"

#: /tmp/lok-qml-async-imageprovider-build/po/com.ubuntu.docviewer.desktop.in.in.h:2
msgid "documents;viewer;pdf;reader;"
msgstr "documents;viewer;pdf;reader;asiakirjat;dokumentit;katselin;lukija;"

#~ msgid "File does not exist"
#~ msgstr "Tiedostoa ei ole olemassa"

#~ msgid "No document found"
#~ msgstr "Asiakirjaa ei löytynyt"

#~ msgid "Hide table of contents"
#~ msgstr "Piilota sisällysluettelo"
